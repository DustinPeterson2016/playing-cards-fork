
// Ryan Appel
// Playing Cards

#include <iostream>
#include <conio.h>

using namespace std;

// enums for rank & suit
enum Rank
{
	Two = 2, Three, Four, Five, Six, Seven,
	Eight, Nine, Ten, Jack, Queen, King, Ace
};

enum Suit
{
	Spades, Hearts, Clubs, Diamonds
};


// struct for card
struct Card
{
	Rank rank;
	Suit suit;
};


int main()
{
	Card a;
	a.rank = Jack;
	a.suit = Hearts;

	Card b;
	b.rank = Ace;
	b.suit = Spades;

	Card c;
	c.rank = Ace;
	c.suit = Diamonds;

	Card d;
	d.rank = Three;
	d.suit = Diamonds;

	(void)_getch();
	return 0;
}
